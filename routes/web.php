<?php

use App\Http\Controllers\FizBusController;
use App\Http\Controllers\JabatanController;
use App\Http\Controllers\KontrakController;
use App\Http\Controllers\PegawaiController;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider and all of them will
| be assigned to the "web" middleware group. Make something great!
|
*/

Route::get('/', function () {
    return view('admin.layout.master');
});

Route::get('test-2',[FizBusController::class,'index'])->name('fizBus');


Route::prefix('admin')->group(function () {
    Route::resource('pegawai',PegawaiController::class);
    Route::resource('jabatan',JabatanController::class)->except('show');
    Route::resource('kontrak',KontrakController::class)->except('show');
});

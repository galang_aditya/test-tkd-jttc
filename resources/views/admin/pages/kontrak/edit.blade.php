@extends('admin.layout.master')
@section('main')
@section('breadcrumb')
<li class="breadcrumb-item"><a href="{{ route('kontrak.index') }}">Kontrak</a></li>
<li class="breadcrumb-item active">Edit Data Kontrak</li>
@endsection
<div class="card">
    <div class="card-body">
      <h5 class="card-title">Form Edit Data Kontrak</h5>

      <!-- General Form Elements -->
      <form action="{{ route('kontrak.update',$kontrak) }}" method="POST">
        @csrf @method("PUT")
        <div class="row mb-3">
          <label for="inputText" class="col-sm-2 col-form-label">Nama Kontrak</label>
          <div class="col-sm-10">
            <input type="text" class="form-control" name="nama_kontrak" value="{{ $kontrak->nama_kontrak }}">
          </div>
        </div>
        <div class="row mb-3">
            <label for="inputText" class="col-sm-2 col-form-label">Tanggal Mulai Kontrak</label>
            <div class="col-sm-10">
              <input type="date" class="form-control" name="tgl_mulai" value="{{ $kontrak->tgl_mulai }}">
            </div>
          </div>
          <div class="row mb-3">
            <label for="inputText" class="col-sm-2 col-form-label">Tanggal Selesai Kontrak</label>
            <div class="col-sm-10">
              <input type="date" class="form-control" name="tgl_selesai" value="{{ $kontrak->tgl_selesai }}">
            </div>
          </div>
        <div class="text-center">
          <div class="col-sm-10">
            <button type="submit" class="btn btn-primary">Update Kontrak</button>
          </div>
        </div>

      </form><!-- End General Form Elements -->

    </div>
</div>
@endsection

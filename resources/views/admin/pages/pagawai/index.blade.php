@extends('admin.layout.master')
@section('main')
@section('breadcrumb')
<li class="breadcrumb-item active">Pagawai</li>
@endsection
    <section class="section">
        <div class="row">
          <div class="col-lg-12">

            <div class="card">
              <div class="card-body">
                <h5 class="card-title">Tabel Data Pegawai</h5>
                <div style="float: left; margin-bottom: 10px">
                    <a href="{{ route('pegawai.create') }}">
                        <button type="button" class="btn btn-primary"><i class="bi bi-clipboard2-plus"></i>Tambah Pegawai</button>
                    </a>
                  </div>
                <!-- Table with stripped rows -->
                <table id="example" class="table table-striped table-bordered" style="width:100%">
                  <thead>
                    <tr>
                      <th scope="col">No</th>
                      <th scope="col">Nama</th>
                      <th scope="col">Jabatan</th>
                      <th scope="col">Tanggal Kontrak</th>
                      <th scope="col">Tanggal Lahir</th>
                      <th scope="col">Jenis Kelamin</th>
                      <th scope="col">Agama</th>
                      <th scope="col">Action</th>
                    </tr>
                  </thead>
                  @php
                      $no = 1;
                  @endphp
                  <tbody>
                    @forelse ( $pegawais as $pegawai )
                    <tr>
                        <td>{{ $no++ }}</td>
                        <td>{{ $pegawai->nama_pegawai }}</td>
                        <td>{{ $pegawai->jabatans->nama_jabatan }}</td>
                        <td>{{ $pegawai->kontraks->nama_kontrak }}</td>
                        <td>{{ $pegawai->tgl_lahir }}</td>
                        <td>{{ $pegawai->jenis_kelamin }}</td>
                        <td>{{ $pegawai->agama }}</td>
                        <td>
                            <a href="{{ route('pegawai.edit',$pegawai) }}">
                                <button type="button" class="btn btn-warning"><i
                                        class="bi bi-pencil"></i></button>
                            </a>
                            <button type="button" class="btn btn-danger delete-btn" data-toggle="modal" data-target="#exampleModal">
                                <i class="bi bi-trash"></i>
                            </div>
                        </td>
                        <div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                            <div class="modal-dialog" role="document">
                              <div class="modal-content">
                                <div class="modal-header">
                                  <h5 class="modal-title" id="exampleModalLabel">Modal title</h5>
                                  <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                    <span aria-hidden="true">&times;</span>
                                  </button>
                                </div>
                                <div class="modal-body">
                                  Apakah Anda yakin Ingin Menghapus data ini ?
                                </div>
                                <div class="modal-footer">
                                  <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                                  <form action="{{ route('pegawai.destroy',$pegawai) }}" method="POST">
                                      @csrf @method('DELETE')
                                      <button type="submit" class="btn btn-danger">Hapus Data</button>
                                  </form>
                                </div>
                              </div>
                            </div>
                          </div>
                    </tr>
                    @empty

                    @endforelse

                  </tbody>
                </table>
                <!-- End Table with stripped rows -->

              </div>
            </div>

          </div>
      </section>
</div>
@endsection



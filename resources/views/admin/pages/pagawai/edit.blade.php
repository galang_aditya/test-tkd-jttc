@extends('admin.layout.master')
@section('main')
@section('breadcrumb')
<li class="breadcrumb-item"><a href="{{ route('pegawai.index') }}">Pegawai</a></li>
<li class="breadcrumb-item active">Edit Data Pegawai</li>
@endsection
<div class="card">
    <div class="card-body">
      <h5 class="card-title">Form Edit Data Kontrak</h5>

      <!-- General Form Elements -->
      <form action="{{ route('pegawai.update',$pegawai) }}" method="POST">
        @csrf @method('PUT')
        <div class="row mb-3">
          <label for="inputText" class="col-sm-2 col-form-label">Nama Pegawai</label>
          <div class="col-sm-10">
            <input type="text" class="form-control" name="nama_pegawai" value="{{ $pegawai->nama_pegawai }}">
          </div>
        </div>
        <div class="row mb-3">
            <label class="col-sm-2 col-form-label">Jabatan</label>
            <div class="col-sm-10">
              <select class="form-select" name="id_jabatan" aria-label="Default select example">
                <option selected value="{{ $pegawai->id_jabatan }}">{{ $pegawai->jabatans->nama_jabatan }}</option>
                @foreach ($jabatans as $jabatan)
                <option value="{{ $jabatan->id }}">{{ $jabatan->nama_jabatan }}</option>
                @endforeach
              </select>
            </div>
          </div>
          <div class="row mb-3">
            <label class="col-sm-2 col-form-label">Jenis Kontrak</label>
            <div class="col-sm-10">
              <select class="form-select" name="id_kontrak" aria-label="Default select example">
                <option selected value="{{ $pegawai->id_kontrak }}">{{ $pegawai->kontraks->nama_kontrak }}</option>
                @foreach ($kontraks as $kontrak)
                <option value="{{ $kontrak->id }}">{{ $kontrak->nama_kontrak }}</option>
                @endforeach
              </select>
            </div>
          </div>
          <div class="row mb-3">
            <label for="inputText" class="col-sm-2 col-form-label">Tanggal Lahir</label>
            <div class="col-sm-10">
              <input type="date" class="form-control" name="tgl_lahir" value="{{ $pegawai->tgl_lahir }}">
            </div>
          </div>
          <div class="row mb-3">
            <label class="col-sm-2 col-form-label">Jenis Kelamin</label>
            <div class="col-sm-10">
              <select class="form-select" name="jenis_kelamin" aria-label="Default select example">
                <option selected value="{{ $pegawai->jenis_kelamin }}">{{ $pegawai->jenis_kelamin }}</option>
                <option value="Laki-Laki">Laki-Laki</option>
                <option value="Perempuan">Perempuan</option>
              </select>
            </div>
          </div>
          <div class="row mb-3">
            <label for="inputText" class="col-sm-2 col-form-label">Agama</label>
            <div class="col-sm-10">
              <input type="text" class="form-control" name="agama" value="{{ $pegawai->agama }}">
            </div>
          </div>
        <div class="text-center">
          <div class="col-sm-10">
            <button type="submit" class="btn btn-primary">Update Pegawai</button>
          </div>
        </div>

      </form><!-- End General Form Elements -->

    </div>
</div>
@endsection

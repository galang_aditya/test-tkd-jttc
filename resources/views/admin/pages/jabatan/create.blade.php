@extends('admin.layout.master')
@section('main')
@section('breadcrumb')
<li class="breadcrumb-item"><a href="{{ route('pegawai.index') }}">Jabatan</a></li>
<li class="breadcrumb-item active">Tambah Data Jabatan</li>
@endsection
<div class="card">
    <div class="card-body">
      <h5 class="card-title">Form Tambah Data Jabatan</h5>

      <!-- General Form Elements -->
      <form action="{{ route('jabatan.store') }}" method="POST">
        @csrf
        <div class="row mb-3">
          <label for="inputText" class="col-sm-2 col-form-label">Nama Jabatan</label>
          <div class="col-sm-10">
            <input type="text" class="form-control" name="nama_jabatan">
          </div>
        </div>
        <div class="row mb-3">
            <label for="inputText" class="col-sm-2 col-form-label">Kode Jabatan</label>
            <div class="col-sm-10">
              <input type="text" class="form-control" name="kode_jabatan">
            </div>
          </div>
        <div class="text-center">
          <div class="col-sm-10">
            <button type="submit" class="btn btn-primary">Simpan Jabatan</button>
          </div>
        </div>

      </form><!-- End General Form Elements -->

    </div>
</div>
@endsection

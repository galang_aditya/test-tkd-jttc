@extends('admin.layout.master')
@section('main')
@section('breadcrumb')
<li class="breadcrumb-item active">Jabatan</li>
@endsection
<section class="section">
    <div class="row">
        <div class="col-lg-12">

            <div class="card">
                <div class="card-body">
                    <h5 class="card-title">Tabel Data Jabatan</h5>
                    <div style="float: left; margin-bottom: 10px">
                        <a href="{{ route('jabatan.create') }}">
                            <button type="button" class="btn btn-primary"><i class="bi bi-clipboard2-plus"></i>Tambah
                                Jabatan</button>
                        </a>
                    </div>
                    <!-- Table with stripped rows -->
                    <table id="example" class="table table-striped table-bordered" style="width:100%">
                        <thead>
                            <tr>
                                <th scope="col">No</th>
                                <th scope="col">Nama Jabatan</th>
                                <th scope="col">Kode Jabatan</th>
                                <th scope="col">Action</th>
                            </tr>
                        </thead>
                        @php
                        $no = 1;
                        @endphp
                        <tbody>
                            @forelse ( $jabatans as $jabatan )
                            <tr>
                                <td>{{ $no++ }}</td>
                                <td>{{ $jabatan->nama_jabatan }}</td>
                                <td>{{ $jabatan->kode_jabatan }}</td>
                                <td>
                                    <a href="{{ route('jabatan.edit',$jabatan) }}">
                                        <button type="button" class="btn btn-warning"><i
                                                class="bi bi-pencil"></i></button>
                                    </a>
                                    <button type="button" class="btn btn-danger delete-btn" data-toggle="modal" data-target="#exampleModal">
                                        <i class="bi bi-trash"></i>
                                    </div>
                                </td>
                            </tr>
                            <!-- Modal -->
                            <div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                              <div class="modal-dialog" role="document">
                                <div class="modal-content">
                                  <div class="modal-header">
                                    <h5 class="modal-title" id="exampleModalLabel">Modal title</h5>
                                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                      <span aria-hidden="true">&times;</span>
                                    </button>
                                  </div>
                                  <div class="modal-body">
                                    Apakah Anda yakin Ingin Menghapus data ini ?
                                  </div>
                                  <div class="modal-footer">
                                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                                    <form action="{{ route('jabatan.destroy',$jabatan) }}" method="POST">
                                        @csrf @method('DELETE')
                                        <button type="submit" class="btn btn-danger">Hapus Data</button>
                                    </form>
                                  </div>
                                </div>
                              </div>
                            </div>
                            @empty

                            @endforelse

                        </tbody>
                    </table>
                    <!-- End Table with stripped rows -->

                </div>
            </div>

        </div>
</section>
</div>
@endsection

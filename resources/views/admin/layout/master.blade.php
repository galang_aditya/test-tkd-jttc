<!DOCTYPE html>
<html lang="en">

<head>
  <meta charset="utf-8">
  <meta content="width=device-width, initial-scale=1.0" name="viewport">

  <title>Dashboard - NiceAdmin Bootstrap Template</title>
  <meta content="" name="description">
  <meta content="" name="keywords">

  <!-- Favicons -->
  <link href="{{asset ('assets-backend') }}/img/favicon.png" rel="icon">
  <link href="{{asset ('assets-backend') }}/img/apple-touch-icon.png" rel="apple-touch-icon">

  <!-- Google Fonts -->
  <link href="https://fonts.gstatic.com" rel="preconnect">
  <link href="https://fonts.googleapis.com/css?family=Open+Sans:300,300i,400,400i,600,600i,700,700i|Nunito:300,300i,400,400i,600,600i,700,700i|Poppins:300,300i,400,400i,500,500i,600,600i,700,700i" rel="stylesheet">

  <!-- Vendor CSS Files -->
  <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@4.0.0/dist/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">
  <link href="{{asset ('assets-backend') }}/vendor/bootstrap/css/bootstrap.min.css" rel="stylesheet">
  <link href="{{asset ('assets-backend') }}/vendor/bootstrap-icons/bootstrap-icons.css" rel="stylesheet">
  <link href="{{asset ('assets-backend') }}/vendor/boxicons/css/boxicons.min.css" rel="stylesheet">
  <link href="{{asset ('assets-backend') }}/vendor/quill/quill.snow.css" rel="stylesheet">
  <link href="{{asset ('assets-backend') }}/vendor/quill/quill.bubble.css" rel="stylesheet">
  <link href="{{asset ('assets-backend') }}/vendor/remixicon/remixicon.css" rel="stylesheet">
  <link href="{{asset ('assets-backend') }}/vendor/simple-datatables/style.css" rel="stylesheet">
  <script src="https://code.jquery.com/jquery-3.2.1.slim.min.js" integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN" crossorigin="anonymous"></script>
<script src="https://cdn.jsdelivr.net/npm/popper.js@1.12.9/dist/umd/popper.min.js" integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q" crossorigin="anonymous"></script>
<script src="https://cdn.jsdelivr.net/npm/bootstrap@4.0.0/dist/js/bootstrap.min.js" integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl" crossorigin="anonymous"></script>

  @yield('css')

  <!-- Template Main CSS File -->
  <link href="{{asset ('assets-backend') }}/css/style.css" rel="stylesheet">

  <!-- =======================================================
  * Template Name: NiceAdmin
  * Updated: Mar 09 2023 with Bootstrap v5.2.3
  * Template URL: https://bootstrapmade.com/nice-admin-bootstrap-admin-html-template/
  * Author: BootstrapMade.com
  * License: https://bootstrapmade.com/license/
  ======================================================== -->
</head>

<body>

  <!-- ======= Header ======= -->
    @include('admin.layout.header')
  <!-- End Header -->

  <!-- ======= Sidebar ======= -->
    @include('admin.layout.sidebar')
  <!-- End Sidebar-->

  <main id="main" class="main">

    <div class="pagetitle">
      <h1>Dashboard</h1>
      <nav>
        <ol class="breadcrumb">
          <li class="breadcrumb-item"><a href="index.html">Home</a></li>
          {{--  <li class="breadcrumb-item active">Dashboard</li>  --}}
          @yield('breadcrumb');
        </ol>
      </nav>
    </div><!-- End Page Title -->

    <section class="section dashboard">
      <div class="row">
        @yield('main')
      </div>
    </section>

  </main><!-- End #main -->

  <!-- ======= Footer ======= -->
  {{--  @include('admin.layout.footer')  --}}
  <!-- End Footer -->

  <a href="#" class="back-to-top d-flex align-items-center justify-content-center"><i class="bi bi-arrow-up-short"></i></a>

  <!-- Vendor JS Files -->
  <script src="{{asset ('assets-backend') }}/vendor/apexcharts/apexcharts.min.js"></script>
  <script src="{{asset ('assets-backend') }}/vendor/bootstrap/js/bootstrap.bundle.min.js"></script>
  <script src="{{asset ('assets-backend') }}/vendor/chart.js/chart.umd.js"></script>
  <script src="{{asset ('assets-backend') }}/vendor/echarts/echarts.min.js"></script>
  <script src="{{asset ('assets-backend') }}/vendor/quill/quill.min.js"></script>
  <script src="{{asset ('assets-backend') }}/vendor/simple-datatables/simple-datatables.js"></script>
  <script src="{{asset ('assets-backend') }}/vendor/tinymce/tinymce.min.js"></script>
  <script src="{{asset ('assets-backend') }}/vendor/php-email-form/validate.js"></script>

  @yield('js');

  <!-- Template Main JS File -->
  <script src="{{asset ('assets-backend') }}/js/main.js"></script>

</body>

</html>

<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class PegawaiStoreRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     */
    public function authorize(): bool
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array<string, \Illuminate\Contracts\Validation\ValidationRule|array|string>
     */
    public function rules(): array
    {
        return [
            //
            // 'nama_kontrak' => 'required',
            // 'id_jabatan' => 'required',
            // 'id_kontrak' => 'required',
            // 'tgl_lahir' => 'required',
            // 'jenis_kelamin' => 'required',
            // 'agama' => 'required'
        ];
    }
}

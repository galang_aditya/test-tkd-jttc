<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Kontrak;
use App\Http\Requests\KontrakStoreRequest;
use App\Http\Requests\KontrakUpdateRequest;

class KontrakController extends Controller
{
    /**
     * Display a listing of the resource.
     */
    public function index()
    {
        $kontraks = Kontrak::all();
        return view('admin.pages.kontrak.index',compact('kontraks'));
        //
    }

    /**
     * Show the form for creating a new resource.
     */
    public function create()
    {
        //
        return view('admin.pages.kontrak.create');
    }

    /**
     * Store a newly created resource in storage.
     */
    public function store(KontrakStoreRequest $request, Kontrak $kontrak)
    {
        //
        $data = $request->safe()->all();
        $kontrak = new Kontrak($data);
        $kontrak->save();

        return redirect()->route('kontrak.index');
    }

    /**
     * Display the specified resource.
     */
    public function show(string $id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     */
    public function edit(Kontrak $kontrak)
    {
        //
        return view('admin.pages.kontrak.edit',compact('kontrak'));
    }

    /**
     * Update the specified resource in storage.
     */
    public function update(KontrakUpdateRequest $request,Kontrak  $kontrak)
    {
        //
        $kontrak->update($request->all());
        return redirect()->route('kontrak.index');
    }

    /**
     * Remove the specified resource from storage.
     */
    public function destroy(Kontrak $kontrak)
    {
        //
        $kontrak->delete();
        return redirect()->route('kontrak.index');
    }
}

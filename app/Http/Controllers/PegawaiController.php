<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Pegawai;
use App\Models\Kontrak;
use App\Models\JabatanPegawai;
use App\Http\Requests\PegawaiStoreRequest;
use App\Http\Requests\PegawaiUpdateRequest;

class PegawaiController extends Controller
{
    /**
     * Display a listing of the resource.
     */
    public function index()
    {
        //
        $pegawais = Pegawai::all();
        return view('admin.pages.pagawai.index',compact('pegawais'));
    }

    /**
     * Show the form for creating a new resource.
     */
    public function create()
    {
        //
        $jabatans= JabatanPegawai::all();
        $kontraks = Kontrak::all();
        return view('admin.pages.pagawai.create',compact('jabatans','kontraks'));
    }

    /**
     * Store a newly created resource in storage.
     */
    public function store(PegawaiStoreRequest $request,Pegawai $pegawai)
    {
        //

        $data = $request->all();
        $pegewai = new Pegawai ($data);
        $pegewai->save();
        return redirect()->route('pegawai.index');
    }

    /**
     * Display the specified resource.
     */
    public function show(string $id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     */
    public function edit(Pegawai $pegawai)
    {
        //
        $jabatans= JabatanPegawai::all();
        $kontraks= Kontrak::all();
        return view('admin.pages.pagawai.edit',compact('pegawai','kontraks','jabatans'));

    }

    /**
     * Update the specified resource in storage.
     */
    public function update(PegawaiUpdateRequest $request,Pegawai $pegawai)
    {
        //
        $pegawai->update($request->all());
        return redirect()->route('pegawai.index');
    }

    /**
     * Remove the specified resource from storage.
     */
    public function destroy(Pegawai $pegawai)
    {
        //
        $pegawai->delete();
        return redirect()->route('pegawai.index');
    }
}

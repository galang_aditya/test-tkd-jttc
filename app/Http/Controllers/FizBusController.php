<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class FizBusController extends Controller
{
    //
    public function index(){
        for ($i = 1; $i <= 100; $i++) {
            if ($i % 3 === 0 && $i % 5 === 0) {
                echo "FizzBuzz";
            } elseif ($i % 3 === 0) {
                echo "Fizz";
            } elseif ($i % 5 === 0) {
                echo "Buzz";
            } else {
                echo $i;
            }
            echo "<br>";
        }
    }
}

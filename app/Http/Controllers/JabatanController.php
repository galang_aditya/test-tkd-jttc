<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\JabatanPegawai;
use App\Http\Requests\JabatanStoreRequest;
use App\Http\Requests\JabatanUpdateRequest;

class JabatanController extends Controller
{
    /**
     * Display a listing of the resource.
     */
    public function index()
    {
        //
        $jabatans = JabatanPegawai::all();
        return view('admin.pages.jabatan.index',compact('jabatans'));

    }

    /**
     * Show the form for creating a new resource.
     */
    public function create()
    {
        //
        return view('admin.pages.jabatan.create');
    }

    /**
     * Store a newly created resource in storage.
     */
    public function store(JabatanStoreRequest $request )
    {
        //
        $data = $request->safe()->all();
        $jabatan = new JabatanPegawai($data);
        $jabatan->save();

        return redirect()->route('jabatan.index');
    }

    /**
     * Display the specified resource.
     */
    public function show(string $id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     */
    public function edit(JabatanPegawai $jabatan)
    {
        //
        return view('admin.pages.jabatan.edit', compact('jabatan'));
    }

    /**
     * Update the specified resource in storage.
     */
    public function update(JabatanUpdateRequest $request,JabatanPegawai  $jabatan)
    {
        //
        $jabatan->update($request->all());
        return redirect()->route('jabatan.index');
    }

    /**
     * Remove the specified resource from storage.
     */
    public function destroy(JabatanPegawai $jabatan)
    {
        //
        $jabatan->delete();
        return redirect()->route('jabatan.index');
    }
}

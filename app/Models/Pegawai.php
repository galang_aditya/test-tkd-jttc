<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use App\Models\JabatanPegawai;
use App\Models\Kontrak;

class Pegawai extends Model
{
    use HasFactory;

    protected $fillable = [
        'id_jabatan',
        'id_kontrak',
        'nama_pegawai',
        'tgl_lahir',
        'jenis_kelamin',
        'agama'
    ];

    public function jabatans(){
        return $this->belongsTo(JabatanPegawai::class,'id_jabatan','id');
    }
    public function kontraks(){
        return $this->belongsTo(Kontrak::class,'id_kontrak','id');
    }
}
